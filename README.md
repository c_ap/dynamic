Team Dynamic 

Project Title:
Web application for bug tracking

Members:
Moraru Radu
Negru Ovidiu
Popescu Teodor
Puican Cristian

Project Manager: Puican Cristian
Product Manager: Popescu Teodor
Developer: Moraru Radu
Developer: Negru Ovidiu



API Documentation:

!In folder PostmanRoutes, there are examples of how the app should work.
We will specify for each route what image to see!

loggedUser middleware is used to check that a user is logged, before trying to access certain routes

1) /reset
    *GET route
    *image: ResetDatabaseRoute
    *it is used to delete all entries in the database
    *used for development
    
    
2) /register
    *POST route
    *used for user creation
    *image: RegisterUserRoute
    *POSTMAN body example:
        {
    	"username":"ovidiu@gmail.com",
    	"password":"pass3"
        }
    

3) /login
    *POST route
    *used for user authentication
    *image: LoginRoute
    *POSTMAN body example:
        {
    	"username":"ovidiu@gmail.com",
    	"password":"pass3"
        }
    

4) /logout
    *GET route
    *used for signing off the logged user
    *image: LogoutRoute
    *NO POSTMAN body required
    

5) /users
    *GET route
    *used for displaying all the created users
    *used for debugging purposes
    *image: GetUsersRoute
    *NO POSTMAN body required
    
    
6) /projects
    *POST route
    *used to register a project
    *image: PostProjectCreationRoute
    *POSTMAN body example:
        {
    	"name":"dynamic",
    	"repo_link": "dynamic@git.ro",
    	"description":"Bug tracking project",
    	"members":"cristi@gmail.com,teo@gmail.com"
        }
    
    
7) /projects
    *GET route
    *used for displaying all registered projects
    *image: GetProjectsRoute
    *NO POSTMAN body required
    

8) /members
    *GET route
    *used to display all registered project members
    *used for debugging puropses
    *image: GetProjectMemberRoute
    *NO POSTMAN body required
    
    
9) /projects/:id
    *POST route
    *used by a non PM member to register as a tester on a project identified by :id
    *image: PostAddTesterRoute
    *NO POSTMAN body required 
    *used with this route: /projects/1 (for projectId = 1)
    

10) /projects/:id/bugs
    *POST route
    *used by a tester to add a bug to the project identified by :id
    *image: PostAddBugRoute
    *used with this route: /projects/1/bugs
    *POSTMAN body example:
        {
        	"severity":"CRITICAL",
        	"priority":"HIGH",
        	"description":"Test description",
        	"issue_link":"issue@git.com"
        }
    

11) /bugs
    *GET route
    *used for displaying all registered bugs
    *used for debugging purposes
    *image: GetBugsRoute
    *NO POSTMAN body required
    
    
12) /projects/bugs
    *GET route
    *used for displaying all bugs of a logged DEV (all bugs for the projects he/she is PM)
    *image: GetAllBugsForDevRoute
    *NO POSTMAN body required
    
    
13) /bugs/:id
    *PUT route
    *used by a DEV to assign itself a bug with id :id to resolve
    *image: UpdateBugAllocatedtoDevRoute
    *used with this route: /bugs/1
    *NO POSTMAN body required
    
    
14) /bugs/:id/solution
    *PUT route
    *used by the DEV who is assigned to the bug with id :id, to add a resolution
    *image: UpdateResolvedBugRoute
    *used with the route: /bugs/1/solution
    *POSTMAN body example:
        {
        	"status": "solved",
        	"solved_link":"https://mygit.dynamic.com"
        }
        
        
15) /users
    *DELETE route
    *used to delete the currently logged user
    *image: DeleteUserRoute
    *NO POSTMAN body required
   
    
16) /projects/:id
    *DELETE route
    *used by the PM who created the project with :id to delete it
    *image: DeleteProjectRoute
    *used with route: /projects/1
    *NO POSTMAN body required
    

17) /bugs/:id
    *DELETE route
    *used by the PM who created the project to delete the bug with id :id
    *image: DeleteBugRoute
    *used with route: /bugs/1
    *NO POSTMAN body required