import React from "react";
import axios from "axios";
import ProjectList from "./ProjectList"
const SERVER = "http://3.17.65.123:8080";

class Login extends React.Component{
    
    constructor(props){
    super(props);
    this.state ={
      username: "",
      password: "",
      user: null
    };
    
    this.component = null;
    
  }
  
  
  handleChangeUsername = (event)=>{
    this.setState({
      username: event.target.value
    });
  }
  
  
  handleChangePassword = (event)=>{
    this.setState({
      password: event.target.value
    });
  }
  
  logUser = () =>{
    let user = {
      username: this.state.username,
      password: this.state.password
    };
    // console.log('aici');
    axios.post(`${SERVER}/register`,user).then(res => {
        this.setState({user:res.data});
        // this.component = <div>Tralalaa</div>;
        // console.log('aici',this.state.user);
    });
    
  }
  

  render() {
    return (
      <div className="App">
              {
                this.state.user ? <ProjectList userId={this.state.user.id}/> : 
                <div className="container mt-5">
                   <input className="form-control mb-2" name="username" type="email" placeholder="Email" onChange={this.handleChangeUsername}/>
                   <input className="form-control mb-2" name="password" type="password" placeholder="Password" onChange={this.handleChangePassword}/>
                   <button className="btn btn-primary" onClick={this.logUser}>Register</button>
                </div>
              }
      </div>
    );
  }
}


export default Login;