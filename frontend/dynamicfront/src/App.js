import React, { Component } from 'react';
import './App.css';
import ProjectList from "./ProjectList";
import Login from "./Login"
import Register from "./Register"
import "./bootstrap.css"


class App extends Component {
  
  constructor(){
    super()
    this.state = {
      account : <div className="container d-flex justify-content-around mt-5"><button className="btn btn-info" onClick={this.clickLogin}>Login</button>
                <button className="btn btn-info" onClick={this.clickRegister}>SignUp</button></div>
    }
  }
  
  clickLogin = () =>{
    this.setState({
      account : <Login />
    })
  }
  
  clickRegister = () => {
    this.setState({
      account: <Register />
    })
  }
  
  render(){
    return <div>
      
      {this.state.account}
      </div>
  }
  
}

export default App;
