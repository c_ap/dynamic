import React from "react";
import axios from "axios";
import ProjectMain from "./ProjectMain";
import "./bootstrap.css"
const SERVER = "http://3.17.65.123:8080";

    

class ProjectList extends React.Component {
    
    constructor(props){
        super(props);
        
        this.state = {
            projects: [],
            name: "",
            repo_link: "",
            description: "",
            userId : this.props.userId,
            members: "",
            button: ""
        };
        
        console.log(this.props.userId);
    
    this.projectMain = new ProjectMain();
    
    this.handleChange = (e) => {
            this.setState({
                [e.target.name] : e.target.value
            });
        };
        
    this.bringProjects = () => {
        this.projectMain.getProjects();
        this.projectMain.emitter.addListener("GET_PROJECTS_SUCCESS", () => {
            this.setState({
                projects : this.projectMain.projects
            });
        });
    };

    }
    
    componentDidMount(){
        this.bringProjects();
    }
    
    click = () =>{
            
          let newProject = {
          name: this.state.name,
          repo_link: this.state.repo_link,
          description:this.state.description,
          userId: this.props.userId,
          members: this.state.members
          };
          axios.post(`${SERVER}/projects`, newProject).then(res => {
                this.bringProjects();
          });
          
      
    }
    
    
    render(){
        let items=this.state.projects.map((project,index)=>{
                return <tr key={project.id}>
                <td>{project.id}</td>
                <td>{project.name}</td>
                <td>{project.description}</td>
                <td>{project.repo_link}</td>
              
                    </tr>
            
            
        });
        return <div>
                
            
            <div className="container"> 
            <div className="panel panel-default p50 uth-panel">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Project id</th>
                            <th>Project name</th>
                            <th>Project description</th>
                            <th>Project repo</th>

                        </tr>
                    </thead>
                    <tbody>
                   {items}
                    </tbody>
                </table>
                
            </div>
        </div>
        <div className="container">
        <input className="form-control mb-2" name="name"  placeholder="Project Name" onChange={this.handleChange}/>
        <input className="form-control mb-2" name="repo_link" placeholder="Repository Link" onChange={this.handleChange}/>
        <input className="form-control mb-2" name="description" placeholder="Project Description" onChange={this.handleChange}/>
        <input className="form-control mb-2" name="members" placeholder="Team Members" onChange={this.handleChange}/>
        <button className="btn btn-primary mb-2" onClick={this.click}>Add Project</button>
        </div>
            </div>
    }
    
}


export default ProjectList;